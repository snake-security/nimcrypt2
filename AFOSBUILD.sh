rm -rf /opt/ANDRAX/nimcrypt2

mkdir /opt/ANDRAX/nimcrypt2

nimble install -y winim nimcrypto docopt ptr_math strenc

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Nimble install... PASS!"
else
  # houston we have a problem
  exit 1
fi

nim c -d=release --cc:gcc --embedsrc=on --hints=on --app=console --out=nimcrypt nimcrypt.nim

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Nim Compile Release... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip nimcrypt

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf nimcrypt syscalls.nim /opt/ANDRAX/nimcrypt2

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
